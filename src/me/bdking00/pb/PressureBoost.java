package me.bdking00.pb;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class PressureBoost extends JavaPlugin implements Listener {

	public void onEnable() {
		Bukkit.getServer().getPluginManager().registerEvents(this, this);
		getConfig().options().copyDefaults(true);
		saveDefaultConfig();

	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e) {
		final Player p = (Player) e.getPlayer();
		if(e.getAction() == Action.PHYSICAL) {
			if(e.getClickedBlock().getType() == Material.WOOD_PLATE) {

				if(e.getClickedBlock().getLocation().getBlock().getRelative(BlockFace.DOWN).getTypeId() == getConfig().getInt("Boost.Block")) {
					p.playSound(p.getLocation(), Sound.valueOf(getConfig().getString("Boost.Sound").toUpperCase()), 10, 1);
					p.teleport(p.getLocation().add(0, .5, 0));
					p.setVelocity(p.getVelocity().add(p.getLocation().getDirection().multiply(getConfig().getInt("Boost.Multiply"))).setY(1));

				} else if(e.getClickedBlock().getLocation().getBlock().getRelative(BlockFace.DOWN).getTypeId() == getConfig().getInt("Effect.Block")) {
					p.playSound(p.getLocation(), Sound.valueOf(getConfig().getString("Effect.Sound").toUpperCase()), 10, 1);
					for(String s : getConfig().getStringList("Effect.Effects")) {
						String [] values = s.replaceAll(" ", s).split(",");

						PotionEffectType type;
						int duration = 10;
						int strength = 2;

						type = PotionEffectType.getByName(values[0].toUpperCase());

						if(values.length > 1)
							duration = Integer .parseInt(values[1]);

						if(values.length > 2)
							strength = Integer .parseInt(values[2]);

						PotionEffect effect = type.createEffect(duration * 20, strength);
						p.addPotionEffect(effect);

					}
				}
			}
		}
	}
}
